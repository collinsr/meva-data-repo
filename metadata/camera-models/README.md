This directory contains camera models for the MEVA KF1 collection.

The KF1 collection took place in two separate events, in March and May of 2018. Between the events, camera positions may have shifted; for the moment the camera models are broken out by collection month. As we refine our models, this organization may change.

This directory includes:

* [2018-03](2018-03) is a directory containing models for exterior cameras for the March 2018 collection event.

* [indoor-gym](indoor-gym) is a directory containing models for the interior gym cameras for the March 2018 collection event.
