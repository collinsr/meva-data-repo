This directory contains various metadata regarding the MEVA data.

Subirectories include:

[camera-models](camera-models): Camera models for the MEVA KF1 collection.

